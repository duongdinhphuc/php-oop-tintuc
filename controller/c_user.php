<?php 
	session_start();
	include('model/m_user.php');
	class C_user
	{
		function dangkiTK($name, $email, $password)
		{
			$m_user = new M_user();
			$id_user = $m_user->dangki($name, $email, $password);
			if($id_user>0)
			{
				$_SESSION['success'] = "Dang ky thanh cong !";
				header('location:dangnhap.php');
				if(isset($_SESSION['error']))
				{
					unset($_SESSION['error']);
				}
			}
			else
			{
				$_SESSION['error'] = "Dang ky that bai !";
				header('location:dangky.php');
			}
		}

		function dangnhapTK($email, $password)
		{
			$m_user = new M_user();
			$user = $m_user->dangnhap($email, $password);
			if($user)
			{
				$_SESSION['user_name'] = $user->name;
				$_SESSION['id_user'] = $user->id;
				header('location:index.php');
				if(isset($_SESSION['user_error']))
				{
					unset($_SESSION['user_error']);
				}
				if(isset($_SESSION['chuadangnhap']))
				{
					unset($_SESSION['chuadangnhap']);
				}
			}
			else
			{
				$_SESSION['user_error'] = "Sai thong tin dang nhap !";
				header('location:dangnhap.php');
			}
		}

		function dangxuat()
		{
			session_destroy();
			header('location:index.php');
		}
	}
 ?>