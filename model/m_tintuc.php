<?php 
	include('database.php');
	class M_tintuc extends database
	{
		function getSlide()
		{
			$sql = "select * from slide";
			$this->setQuery($sql);
			return $this->loadAllRows();
		}

		function getMenu()
		{
			$sql = "select tl.*,group_concat(distinct lt.id,':', lt.Ten,':', lt.TenKhongDau) as loaitin, tt.id as idTin, tt.TieuDe as TieuDeTin, tt.Hinh as HinhTin, tt.TomTat as TomTatTin, tt.TieuDeKhongDau as TieuDeKhongDauTin from theloai tl inner join loaitin lt on lt.idTheLoai=tl.id inner join tintuc tt on tt.idLoaiTin=lt.id group by tl.id";
			$this->setQuery($sql);
			return $this->loadAllRows();
		}

		function getTinTucByIdLoaiTin($idLoaiTin, $vitri=-1, $limit=-1)
		{
			$sql = "select * from tintuc where idLoaiTin = $idLoaiTin";
			if($vitri>-1 && $limit>1)
			{
				$sql .= " limit $vitri,$limit";
			}
			$this->setQuery($sql);
			return $this->loadAllRows(array($idLoaiTin));
		}

		function getTitleById($idLoaiTin)
		{
			$sql = "select Ten from loaitin where id=$idLoaiTin";
			$this->setQuery($sql);
			return $this->loadRow(array($idLoaiTin));
		}

		function getChitietTin($id)
		{
			$sql = "select * from tintuc where id = $id";
			$this->setQuery($sql);
			return $this->loadRow(array($id));
		}

		function getComment($id_tin)
		{
			$sql = "select * from comment where idTinTuc=$id_tin";
			$this->setQuery($sql);
			return $this->loadAllRows(array($id_tin));
		}

		function getRelatedNews($alias)
		{
			$sql = "select tt.*, lt.TenKhongDau as TenKhongDau, lt.id as idLoaitin from tintuc tt inner join loaitin lt on tt.idLoaiTin = lt.id where lt.TenKhongDau = '$alias' limit 0,5";
			$this->setQuery($sql);
			return $this->loadAllRows(array($alias));
		}

		function getAliasLoaitin($id_loaitin)
		{
			$sql = "select TenKhongDau from loaitin where id=$id_loaitin";
			$this->SetQuery($sql);
			return $this->loadRow(array($id_loaitin));
		}

		function getTinNoiBat()
		{
			$sql = "select tt.*, lt.TenKhongDau as TenKhongDau, lt.id as idLoaitin from tintuc tt inner join loaitin lt on tt.idLoaiTin = lt.id where tt.NoiBat = 1 limit 0,5";
			$this->setQuery($sql);
			return $this->loadAllRows();
		}

		function addComment($id_user, $id_tin, $noidung)
		{
			$sql = "insert into comment(idUser,idTinTuc,NoiDung) values(?,?,?)";
			$this->setQuery($sql);
			return $this->execute(array($id_user, $id_tin, $noidung));
		}

		function search($key)
		{
			$sql = "select tt.*,lt.TenKhongDau as ten_khong_dau from tintuc tt inner join loaitin lt on tt.idLoaiTin=lt.id where tt.TieuDe like '%$key%' or tt.TomTat like '%$key%'";
			$this->setQuery($sql);
			return $this->loadAllRows(array($key));
		}

	}
 ?>